#!/bin/bash

set -e

echo "Now running script build.sh"

echo "Source remote git repo"
source <(curl -s https://raw.githubusercontent.com/MaksymSemenykhin/bash_scripts/master/output.sh)

ls -la

echo "Version of NodeJS is $(node -v)"
echo "Version of NPM is $(npm -v)"
echo "Version of REDIS is $(redis-server -v)"
echo "Version of YARN is $(yarn -v)"

# Build
echo "Start builging"
npm install


#Lint
echo "Start Linter"
npm run linter || true

