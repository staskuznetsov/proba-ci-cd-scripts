#!/bin/bash

set -e

# input param
BRANCHN=$1

echo "Now running script deploy.sh"
source <(curl -s https://raw.githubusercontent.com/MaksymSemenykhin/bash_scripts/master/output.sh)

# Deploy Folder
DEPLOYF="/var/www/nodes/$BRANCHN"
mkdir -p "$DEPLOYF"
echo "Deploy Folder: $DEPLOYF"

#Prepare config
sed -i s#%ENV%#\'$BRANCHN\'#g ./ecosystem.config.js
sed -i s#development#$BRANCHN#g ./ecosystem.config.js 

echo "Print ecosystem .js file:"
cat ecosystem.config.js


# Start Deploy
echo "Start Deploy!"
mkdir -p "$DEPLOYF/config"
cp ./config/"$BRANCHN".json "$DEPLOYF/config/local.config.json"

echo "Print app config file"
cat "$DEPLOYF/config/local.config.json"

echo "Copy application to $DEPLOYF"
cp -r . "$DEPLOYF"


echo "Version of PM2 is $(pm2 --version)"
pm2 start

# Test
echo "Run Functional Test"
npm run test
